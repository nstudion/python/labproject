import random
import string
import csv
import sys
import numbers
import sys


def _serial_generator(size=10, chars=string.ascii_lowercase + string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

if __name__ == '__main__':
    size = sys.argv[1]
    print(_serial_generator(int(size)))

    
    
